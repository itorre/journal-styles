def test_dummy():
    assert True

def _figure_frame_benchmark():
    "Checks figure_frame results"
    for journal in journals:
        ff = figure_frame("lMs", "lMs", journal=journal)
        assert ff.figsize == standard_figsize(journals[journal])
        assert ff.rects[0] == standard_rect(journals[journal])