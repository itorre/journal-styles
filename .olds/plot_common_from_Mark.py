# -*- coding: utf-8 -*-
"""
Common plot setup
"""

from pylab import *
import noe
from noe import *

from matplotlib import rcParams

import plot_common


#  Device color coding
# see http://paletton.com/#uid=50r0N0kllll7nlzemlsskleEWl7

color_27 = '#A88000'
color_14 = '#A84700'
color_5 = '#8F003F'
color_vF = '#431DDF'
#color_vF = '#236A62'


# revtex column width is 246/72.27 inches ~ 86 mm.
# Get this point width from latex via \the\columnwidth or
#onecolumnwidth_inches = (246.0)/72.27
#twocolumnwidth_inches = (510.0)/72.27

# Nature Physics, Nature Materials
# onecolumnwidth_inches = 88./25.4
# twocolumnwidth_inches = 170./25.4

# Nature:
# onecolumnwidth_inches = 88./25.4
# twocolumnwidth_inches = 183./25.4

# Science figures would be 1 or 2 columns usually
# Guidelines: http://www.sciencemag.org/authors/instructions-preparing-initial-manuscript
onecolumnwidth_inches = 2.25
twocolumnwidth_inches = 4.75
threecolumnwidth_inches = 7.25 # guessed

# The following LaTeX code prints out relevant parameters:
# "columnwidth is \the\columnwidth."
# "textwidth is \the\textwidth."
# "linewidth is \the\linewidth."
# Put this in a figure or wide figure to get the size, remember latex points are 1/72.27 inches.


rcParams['text.latex.preamble'] = [
       r'\usepackage{amsmath}',
       r'\usepackage{color}',
       r'\usepackage[T1]{fontenc}',
       r'\usepackage{helvet}',    # set the normal font here
       r'\renewcommand{\familydefault}{\sfdefault}',
       r'\renewcommand{\rmdefault}{phv} % Arial', # nothing?
       r'\renewcommand{\sfdefault}{phv} % Arial', # nothing?
       r'\usepackage[T1]{sansmath}',
       r'\sansmath',

       # The following from http://tex.stackexchange.com/questions/195832/replace-several-letters-in-math-font
       r'\DeclareSymbolFont{Greekletters}{OT1}{helvet}{m}{n}',
#       r'\DeclareSymbolFont{greekletters}{OML}{iwona}{m}{it}',
       r'\DeclareMathSymbol{\Delta}{\mathord}{Greekletters}{"01}',
       r'\DeclareMathSymbol{\Omega}{\mathord}{Greekletters}{"0A}',
#
#       r'\renewcommand{\textohm}{\ensuremath{\Omega}}',
       r'\usepackage[detect-all]{siunitx}',
]

axeslinewidth = 0.620 # Science style
# axeslinewidth = 0.250 # Nat style
stdlinewidth = 0.8
biglinewidth = 1.25
arrowlinewidth = 0.625
fontsize_small = 7.0
fontsize_inset = 7.0
fontsize_label = 8.0

modcolor = (0.7,0.2,0.2)
modcolor_faint = (0.925,0.8,0.8)
modcolor_plasonly = (0.4,0.4,0.7) #(0.85,0.6,0.6)

rcParams.update(
                    {'font.size': fontsize_label,
                     'font.family': 'sans-serif',
                     'font.sans-serif': ['Helvetica', 'Arial', 'DejaVuSans', ],
                     'lines.linewidth': stdlinewidth,
                     'lines.markersize': 4.0,
                     'lines.markeredgewidth': stdlinewidth,
                     'axes.linewidth': axeslinewidth,
                     'xtick.major.width': axeslinewidth,
                     'xtick.minor.width': axeslinewidth,
                     'ytick.major.width': axeslinewidth,
                     'ytick.minor.width': axeslinewidth,
                     'xtick.labelsize': 'small',
                     'ytick.labelsize': 'small',
                     'xtick.major.pad': 2,
                     'ytick.major.pad': 1,
                     'figure.figsize': (86.4591/25.4, 70/25.4),
                     'figure.subplot.left':   0.17,
                     'figure.subplot.bottom': 0.17,
                     'figure.subplot.right':  0.95,
                     'figure.subplot.top':    0.88,
                 #    'figure.subplot.hspace':    0.1,
                #     'figure.subplot.vspace':    0.1,
                     'savefig.dpi': 300,
                     'savefig.format': 'pdf',
                     'text.usetex': True,
                     'ps.useafm': True,
                     'pdf.use14corefonts': True,
                     } )

labelxtip = r'$x_{\rm tip}~\text{(\si{\micro\meter})}$'
labelytip = r'$y_{\rm tip}~\text{(\si{\micro\meter})}$'

# DECLARE SOME COLOR MAPS

from matplotlib.colors import LinearSegmentedColormap

_RdBu_data = {'blue': [(0.0, 0.12156862765550613,
0.12156862765550613), (0.10000000000000001, 0.16862745583057404,
0.16862745583057404), (0.20000000000000001, 0.30196079611778259,
0.30196079611778259), (0.29999999999999999, 0.50980395078659058,
0.50980395078659058), (0.40000000000000002, 0.78039216995239258,
0.78039216995239258), (0.5, 1, 1),
(0.59999999999999998, 0.94117647409439087, 0.94117647409439087),
(0.69999999999999996, 0.87058824300765991, 0.87058824300765991),
(0.80000000000000004, 0.76470589637756348, 0.76470589637756348),
(0.90000000000000002, 0.67450982332229614, 0.67450982332229614), (1.0,
0.3803921639919281, 0.3803921639919281)],

    'green': [(0.0, 0.0, 0.0), (0.10000000000000001,
    0.094117648899555206, 0.094117648899555206), (0.20000000000000001,
    0.37647059559822083, 0.37647059559822083), (0.29999999999999999,
    0.64705884456634521, 0.64705884456634521), (0.40000000000000002,
    0.85882353782653809, 0.85882353782653809), (0.5,
    1, 1), (0.59999999999999998,
    0.89803922176361084, 0.89803922176361084), (0.69999999999999996,
    0.77254903316497803, 0.77254903316497803), (0.80000000000000004,
    0.57647061347961426, 0.57647061347961426), (0.90000000000000002,
    0.40000000596046448, 0.40000000596046448), (1.0,
    0.18823529779911041, 0.18823529779911041)],

    'red': [(0.0, 0.40392157435417175, 0.40392157435417175),
    (0.10000000000000001, 0.69803923368453979, 0.69803923368453979),
    (0.20000000000000001, 0.83921569585800171, 0.83921569585800171),
    (0.29999999999999999, 0.95686274766921997, 0.95686274766921997),
    (0.40000000000000002, 0.99215686321258545, 0.99215686321258545),
    (0.5, 1, 1),
    (0.59999999999999998, 0.81960785388946533, 0.81960785388946533),
    (0.69999999999999996, 0.57254904508590698, 0.57254904508590698),
    (0.80000000000000004, 0.26274511218070984, 0.26274511218070984),
    (0.90000000000000002, 0.12941177189350128, 0.12941177189350128),
    (1.0, 0.019607843831181526, 0.019607843831181526)]}
cmmyrb = LinearSegmentedColormap('cmmyrb',_RdBu_data,N=257)


cmsky = LinearSegmentedColormap('sky',
    dict(
        red = [(0.0,   0/255.,   0/255.),
               (0.2,  38/255.,  38/255.),
               (0.4,  75/255.,  75/255.),
               (0.6, 202/255., 202/255.),
               (0.8, 252/255., 252/255.),
               (1.0, 255/255., 255/255.),],
      green = [(0.0,   0/255.,   0/255.),
               (0.2,  41/255.,  41/255.),
               (0.4, 100/255., 100/255.),
               (0.6, 122/255., 122/255.),
               (0.8, 211/255., 211/255.),
               (1.0, 255/255., 255/255.),],
       blue = [(0.0,   0/255.,   0/255.),
               (0.2, 101/255., 101/255.),
               (0.4, 119/255., 119/255.),
               (0.6,  63/255.,  63/255.),
               (0.8,  85/255.,  85/255.),
               (1.0, 255/255., 255/255.),],
        ),N=257)


cmwyko = LinearSegmentedColormap('wyko',
    dict(
        red = [(0.0,   1/255.,   1/255.),
               (1/11.,  19/255.,  19/255.),
               (2/11.,  41/255.,  41/255.),
               (3/11., 63/255., 63/255.),
               (4/11., 90/255., 90/255.),
               (5/11., 116/255., 116/255.),
               (6/11., 141/255., 141/255.),
               (7/11., 165/255., 165/255.),
               (8/11., 187/255., 187/255.),
               (9/11., 206/255., 206/255.),
               (10/11., 232/255., 232/255.),
               (11/11., 254/255., 254/255.),],
      green = [(0.0,   11/255.,   11/255.),
               (1/11.,  31/255.,  31/255.),
               (2/11.,  73/255.,  73/255.),
               (3/11., 134/255., 134/255.),
               (4/11., 201/255., 201/255.),
               (5/11., 250/255., 250/255.),
               (6/11., 252/255., 252/255.),
               (7/11., 204/255., 204/255.),
               (8/11., 133/255., 133/255.),
               (9/11., 74/255., 74/255.),
               (10/11., 31/255., 31/255.),
               (11/11., 13/255., 13/255.),],
       blue = [(0.0,   107/255.,   107/255.),
               (1/11.,  193/255.,  193/255.),
               (2/11.,  242/255.,  242/255.),
               (3/11., 252/255., 252/255.),
               (4/11., 218/255., 218/255.),
               (5/11., 151/255., 151/255.),
               (6/11., 87/255., 87/255.),
               (7/11., 37/255., 37/255.),
               (8/11., 9/255., 9/255.),
               (9/11., 0/255., 0/255.),
               (10/11., 1/255., 1/255.),
               (11/11., 4/255., 4/255.),],
        ),N=257)

cmbgy = LinearSegmentedColormap('bgy',   # Blue Green Yellow for topography
    dict(
        red = [(0.0,   72/255.,   72/255.),
               (0.131356,  52/255.,  52/255.),
               (0.370763,  16/255.,  16/255.),
               (0.5, 239/255., 239/255.),
               (0.57839, 252/255., 252/255.),
               (1., 218/255., 218/255.),],
      green = [(0.0,   82/255.,   82/255.),
               (0.131356,  129/255.,  129/255.),
               (0.370763,  202/255.,  202/255.),
               (0.5, 195/255., 195/255.),
               (0.57839, 194/255., 194/255.),
               (1., 145/255., 145/255.),],
       blue = [(0.0,   174/255.,   174/255.),
               (0.131356,  139/255.,  139/255.),
               (0.370763,  75/255.,  75/255.),
               (0.5, 4/255., 4/255.),
               (0.57839, 0/255., 0/255.),
               (1., 0/255., 0/255.),],
        ),N=257)

#%%

def make_second_scaled_yaxis(ax, scale):
    ax2 = ax.twinx()          # ax2 is the Celsius scale

    def update_ax2(ax):
       y1, y2 = ax.get_ylim()
       ax2.set_ylim(y1*scale, y2*scale)
       ax2.figure.canvas.draw_idle()

    # automatically update ylim of ax2 when ylim of ax1 changes.
    ax.callbacks.connect("ylim_changed", update_ax2)
    update_ax2(ax)
    return ax2

import matplotlib.ticker as ticker

def unify_colorbar(cb,ticklabelpad=0):
    """
    Convenience method for making consistent colorbar appearance
    """
    cb.solids.set_edgecolor("face") ; cb.solids.set_rasterized(True)
    cb.ax.tick_params(length=2,width=0.25,pad=ticklabelpad)
    for s in cb.ax.spines.values():
        s.set_linewidth(0.25)

def print_aspect(ax):
    """
    Print diagnostics about data aspect ratio and figure aspect ratio. Used
    for correctly sizing figures/axes with fixed aspect data.

    You should first set the data limits (xlim, ylim) and the figure size
    correctly.
    """
    xl = ax.get_xlim() ; yl = ax.get_ylim()
    data_aspect = (yl[1]-yl[0])/(xl[1]-xl[0])
    fs = ax.figure.get_size_inches()
    fig_aspect = fs[1]/fs[0]
    ab = ax.get_position(original=True)
    ax_aspect = ab.height/ab.width

    print "Data ratio:",  data_aspect
    print "Fig ratio:",  fig_aspect
    print "Axes fraction ratio:",ax_aspect, "    desired:",data_aspect/fig_aspect





# Colorbar axes locators.
# Note the following code from matplotlib's axes.py may be needed more generally.
#        locator = self.get_axes_locator()
#        if locator:
#            pos = locator(self, renderer)
#            self.apply_aspect(pos)
#        else:
#            self.apply_aspect()
#

def create_cb_top_locator(ax, left=0, right=1, sep_mm=0.5, thick_mm=1):
    """
    Returns a function that can be passed to cbaxes.set_axes_locator().

    The locator will maintain the colorbar at a constant thickness and
    at a constant distance from the main parent axes.

    This is for HORIZONTAL, TOPSIDE colorbars.

    Parameters
    ----------
    ax:
        Parent figure width
    left: 0 to 1
        Fraction of parent figure width
    right: 0 to 1
        Fraction of parent figure width
    """
    def locate(cax, renderer):
        ax.apply_aspect()
        ab = ax.get_position(original=False).get_points()
        vmm = 1/(ax.figure.get_size_inches()[1]*25.4)
        x1 = (ab[0][0]*(1-left) + ab[1][0]*left)
        x2 = (ab[0][0]*(1-right) + ab[1][0]*right)
        y1 = ab[1][1]+sep_mm*vmm
        y2 = ab[1][1]+(sep_mm+thick_mm)*vmm
        return mpl.transforms.Bbox([[x1,y1],[x2,y2]])

    return locate

def create_cb_right_locator(ax, bottom=0, top=1, sep_mm=0.5, thick_mm=1):
    """
    Returns a function that can be passed to cbaxes.set_axes_locator().

    The locator will maintain the colorbar at a constant thickness and
    at a constant distance from the main parent axes.

    This is for VERTICAL, RIGHTSIDE colorbars.

    Parameters
    ----------
    ax:
        Parent figure width
    bottom: 0 to 1
        Fraction of parent figure height
    top: 0 to 1
        Fraction of parent figure height
    """
    def locate(cax, renderer):
        ax.apply_aspect()
        ab = ax.get_position(original=False).get_points()
        hmm = 1/(ax.figure.get_size_inches()[0]*25.4)
        y1 = (ab[0][1]*(1-bottom) + ab[1][1]*bottom)
        y2 = (ab[0][1]*(1-top) + ab[1][1]*top)
        x1 = ab[1][0]+sep_mm*hmm
        x2 = ab[1][0]+(sep_mm+thick_mm)*hmm
        return mpl.transforms.Bbox([[x1,y1],[x2,y2]])

    return locate
