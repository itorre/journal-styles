#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module providing figure styles and dimension for scientific journals"""

__version__ = "0.0.0"
__author__ = "Iacopo Torre"
# from .figures import standard_figsize as standard_figsize
# from .figures import standard_rect as standard_rect
from .figures import Figure_Frame as Figure_Frame
from .figures import Figure_Frame as figure_frame
from .parameters import journals as journals
